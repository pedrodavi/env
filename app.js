const { config, engine } = require('express-edge');
const express = require('express');
const edge = require("edge.js");

const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);

require('dotenv').config();

const bodyParser = require('body-parser');
const fileUpload = require("express-fileupload");
const expressSession = require('express-session');
const connectMongo = require('connect-mongo');
const connectFlash = require("connect-flash");
const compression = require('compression');
const path = require('path')

// protect libs
const helmet = require('helmet');
const cookieParser = require('cookie-parser');

// logger libs
const logger = require('./utils/loggerWinston');
const morgan = require('morgan');

// iniciando express
const app = new express();

// redirecionamento HTTPS
app.use((req, res, next) => { //Cria um middleware onde todas as requests passam por ele 
    if ((req.headers["x-forwarded-proto"] || "").endsWith("http")) //Checa se o protocolo informado nos headers é HTTP 
        res.redirect(`https://${req.headers.host}${req.url}`); //Redireciona pra HTTPS 
    else //Se a requisição já é HTTPS 
        next(); //Não precisa redirecionar, passa para os próximos middlewares que servirão com o conteúdo desejado 
});

// protect
app.use(helmet());
app.disable('x-powered-by');

app.use(helmet.hsts({
    maxAge: 31536000,
    includeSubDomains: true,
    preload: true
}))

app.use(cookieParser());

// compression gzip
app.use(compression());

// ativando logger http do lado do server
app.use(morgan('dev'));

// criando sessão
const mongoStore = connectMongo(expressSession);

app.use(expressSession({
    secret: 'secret-key-aqui',
    maxAge: new Date(Date.now() + 3600000),
    resave: true,
    saveUninitialized: true,
    store: new mongoStore({
        mongooseConnection: mongoose.connection
    })
}));

app.use(connectFlash());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
edge.registerViews(path.join(__dirname, '/views'));
app.use(express.static(__dirname + '/public/'));
app.use(engine);
app.set('views', __dirname + '/views');

// iniciando conexão com banco!
mongoose.connect(`mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}${process.env.DB_HOST}`, 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => logger.info('===> [+] MongoDB ON')) // log pra saber no servidor se conectou ao banco
    .catch(err => logger.info('===> [-] MongoDB OFF: ' + err))

// definindo cache e ambiente
config({ cache: process.env.NODE_ENV === 'production' });

// middleware de autenticação
app.use('*', (req, res, next) => {
    edge.global('auth', req.session.userId)
    next()
});

app.use(fileUpload());

// rota response 404
app.use(function(req, res) {
    res.status(404);
    res.render('404');
});

// start server
app.listen(process.env.PORT || 3000, () => {
    logger.info(`Server listening on port ${process.env.PORT}`);
});

